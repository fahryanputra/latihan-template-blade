<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableQuestionsEngagements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions_engagements', function (Blueprint $table) {
          //Foreign Key
          $table->unsignedBigInteger('pertanyaan_id');
          $table->unsignedBigInteger('profil_id');

          $table->foreign('pertanyaan_id')->references('id')->on('questions');
          $table->foreign('profil_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions_engagements', function (Blueprint $table) {
          $table->dropColumn('pertanyaan_id');
          $table->dropColumn('profil_id');
        });
    }
}
