@extends('layouts.master')

@section('pageTitle')
  Pertanyaan
@endsection

@section('contentTitle')
  Detail Pertanyaan
@endsection

@section('content')
  <h1>{{ $question->judul }}</h1>
  <p>{{ $question->isi }}</p>

  <div class="row mt-4">
    <div class="col-sm-12 text-right">
      <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a>
      <a href="{{ route('pertanyaan.edit', ['pertanyaan' => $question->id]) }}" class="btn btn-primary">Edit</a>
    </div>
  </div>
@endsection
