<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
          //Foreign Keys
          $table->unsignedBigInteger('jawaban_tepat_id')->nullable();
          $table->unsignedBigInteger('profil_id')->nullable();

          $table->foreign('jawaban_tepat_id')->references('id')->on('answers');
          $table->foreign('profil_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
          $table->dropColumn('jawaban_tepat_id');
          $table->dropColumn('profil_id');
        });
    }
}
