<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCommentsOnAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments_on_answers', function (Blueprint $table) {
          //Foreign Keys
          $table->unsignedBigInteger('jawaban_id');
          $table->unsignedBigInteger('profil_id');

          $table->foreign('jawaban_id')->references('id')->on('answers');
          $table->foreign('profil_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments_on_answers', function (Blueprint $table) {
          $table->dropColumn('jawaban_id');
          $table->dropColumn('profil_id');
        });
    }
}
