<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answers', function (Blueprint $table) {
          //Foreign Keys
          $table->unsignedBigInteger('pertanyaan_id');
          $table->unsignedBigInteger('profil_id');

          $table->foreign('pertanyaan_id')->references('id')->on('questions');
          $table->foreign('profil_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function (Blueprint $table) {
          $table->dropColumn('pertanyaan_id');
          $table->dropColumn('profil_id');
        });
    }
}
