@extends('layouts.master')

@section('pageTitle')
  Pertanyaan
@endsection

@section('contentTitle')
  List Pertanyaan
@endsection

@section('content')
  <table class="table table-bordered">
    <thead>
      <tr>
        <th style="width: 10px">id</th>
        <th style="width: 200px">Judul</th>
        <th style="width: 350px">Isi</th>
        <th style="width: 100px">Dibuat</th>
        <th style="width: 100px">Diperbaharui</th>
        <th style="width: 60px">Aksi</th>
      </tr>
    </thead>
    <tbody>
      @forelse($questions as $key => $question)
        <tr>
          <td>{{ $question->id }}</td>
          <td>{{ $question->judul }}</td>
          <td>{{ $question->isi }}</td>
          <td>{{ $question->tanggal_dibuat }}</td>
          <td>{{ $question->tanggal_diperbaharui }}</td>
          <td style="display: flex">
            <a href="{{ route('pertanyaan.show', ['pertanyaan' => $question->id]) }}" class="btn btn-info btn-sm">S</a>
            <a href="{{ route('pertanyaan.edit', ['pertanyaan' => $question->id]) }}" class="btn btn-default btn-sm">E</a>
            <form action="{{ route('pertanyaan.destroy', ['pertanyaan' => $question->id]) }}" method="post">
              @csrf
              @method('DELETE')
              <input type="submit" name="delete" value="D" class="btn btn-danger btn-sm">
            </form>
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="6" align="center">No Data</td>
        </tr>
      @endforelse
    </tbody>
  </table>

  <div class="mt-4">
    <form role="form" action="{{ route('pertanyaan.create') }}" method="get">
      <div class="text-right">
        <button type="submit" name="button" class="btn btn-primary ">Buat</button>
      </div>
    </form>
  </div>
@endsection
