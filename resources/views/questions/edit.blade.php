@extends('layouts.master')

@section('pageTitle')
  Pertanyaan
@endsection

@section('contentTitle')
  Edit Pertanyaan
@endsection

@section('content')
  <form role="form" action="{{ route('pertanyaan.update', ['pertanyaan' => $question->id]) }}" method="post">
    @csrf

    @method('PUT')

    <div class="form-group">
      <label>Judul</label>
      <input name="judul-pertanyaan" type="text" class="form-control" value="{{ $question->judul }}" placeholder="Masukkan judul ...">

      <label>Isi</label>
      <input name="isi-pertanyaan" class="form-control" rows="3" value="{{ $question->isi }}" placeholder="Masukkan isi ...">
    </div>
    <div class="row mt-4">
      <div class="col-sm-12 text-right">
        <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a>
        <button id="btnSearch" type="submit" class="btn btn-primary" Style="width: 100px;">Edit</button>
      </div>
    </div>
  </form>
@endsection
