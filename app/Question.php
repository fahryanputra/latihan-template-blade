<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    // Kalau nama tabelnya tidak sesuai konvensi pakai ini
    // protected $table = 'pertanyaan';

    // Kalau primary keynya bukan 'id' pakai ini
    // protected $primaryKey = 'questions_id'

    public $timestamps = false;

    // whitelist = $fillable
    // blacklist = $guarded
    protected $fillable = ['judul', 'isi', 'tanggal_dibuat'];
}
