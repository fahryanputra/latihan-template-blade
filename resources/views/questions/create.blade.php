@extends('layouts.master')

@section('pageTitle')
  Pertanyaan
@endsection

@section('contentTitle')
  Buat Pertanyaan
@endsection

@section('content')
  <form role="form" action="{{ route('pertanyaan.index') }}" method="post">
    @csrf
    <div class="form-group">
      <label>Judul</label>
      <input name="judul-pertanyaan" type="text" class="form-control" placeholder="Masukkan judul ..." required>

      <label>Isi</label>
      <textarea name="isi-pertanyaan" class="form-control" rows="3" placeholder="Masukkan isi ..." required></textarea>
    </div>
    <div class="row mt-4">
      <div class="col-sm-12 text-right">
        <a href="{{ url()->previous() }}" class="btn btn-default">Kembali</a> 
        <button id="btnSearch" type="submit" class="btn btn-primary" Style="width: 100px;">Post</button>
      </div>
    </div>
  </form>
@endsection
