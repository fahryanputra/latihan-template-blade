<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAnswersEngagements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answers_engagements', function (Blueprint $table) {
          //Foreign Key
          $table->unsignedBigInteger('jawaban_id');
          $table->unsignedBigInteger('profil_id');

          $table->foreign('jawaban_id')->references('id')->on('answers');
          $table->foreign('profil_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers_engagements', function (Blueprint $table) {
          $table->dropColumn('jawaban_id');
          $table->dropColumn('profil_id');
        });
    }
}
