<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use DB;

class PertanyaanController extends Controller
{
    public function index() {
      // Query Builder
      // $posts = DB::table('questions')->get();

      //Eloquent ORM
      $questions = Question::all();

      return view('questions.list', compact('questions'));
    }

    public function create() {
      return view('questions.create');
    }

    public function store(Request $request) {
      // Query Builder
      // $query = DB::table('questions')->insert([
      //                 "judul" => $request["judul-pertanyaan"],
      //                 "isi" => $request["isi-pertanyaan"],
      //                 "tanggal_dibuat" => date('Y-m-d')
      //               ]);

      // Eloquent ORM
      // $question = new Question;
      // $question->judul = $request['judul-pertanyaan'];
      // $question->isi = $request['isi-pertanyaan'];
      // $question->tanggal_dibuat = date('Y-m-d');
      // $question->save();

      // Mass Assignment Eloquent ORM
      Question::create([
        'judul' => $request['judul-pertanyaan'],
        'isi' => $request['isi-pertanyaan'],
        'tanggal_dibuat' => date('Y-m-d')
      ]);

      return redirect('/pertanyaan');
    }

    public function show($id) {
      // Query Builder
      // $post = DB::table('questions')->where('id', $id)->first();

      // Eloquent ORM not primary key
      // $question = Question::where('id', $id)->first();

      // Eloquent ORM using primary key
      $question = Question::find($id);

      return view('questions.show', compact('question'));
    }

    public function edit($id) {
      // Query Builder
      // $post = DB::table('questions')->where('id', $id)->first();

      //Eloquent ORM
      $question = Question::find($id);

      return view('questions.edit', compact('question'));
    }

    public function update($id, Request $request) {
      // Query Builder
      // $query = DB::table('questions')
      //             ->where('id', $id)
      //             ->update([
      //                 'judul'=> $request['judul-pertanyaan'],
      //                 'isi' =>  $request['isi-pertanyaan'],
      //                 'tanggal_diperbaharui' => date('Y-m-d')
      //             ]);

      // Eloquent ORM
      Question::where('id', $id)->update([
        'judul' => $request['judul-pertanyaan'],
        'isi' => $request['isi-pertanyaan'],
        'tanggal_diperbaharui' => date('Y-m-d')
      ]);

      return redirect('/pertanyaan');
    }

    public function destroy($id) {
      // Query Builder
      // $query = DB::table('questions')->where('id', $id)->delete();

      // Eloquent ORM
      Question::destroy($id);

      return redirect('/pertanyaan');
    }
}
